import { takeLatest, call, put, take } from 'redux-saga/effects';
import { UPDATE_POST, FETCH_POSTS } from './constants';
import { postFetched, postUpdated } from './actions';
import { forServer } from '../../utils/formatMessages';
import { webSocket, createEventChannel } from '../../utils/socket';

export function* sendPost({ data }) {
  const channel = yield call(createEventChannel, 'posts');
  setTimeout(() => {
    webSocket.send(forServer('update', data));
  }, 0);
  while (true) { // eslint-disable-line no-constant-condition
    const response = yield take(channel);
    yield put(postUpdated(response));
  }
}

export function* fetchPosts({ data }) {
  const channel = yield call(createEventChannel, 'posts');
  setTimeout(() => {
    webSocket.send(forServer('posts', data));
  }, 0);
  while (true) { // eslint-disable-line no-constant-condition
    const response = yield take(channel);
    yield put(postFetched(response));
  }
}

export default function* defaultSaga() {
  yield takeLatest(FETCH_POSTS, fetchPosts);
  yield takeLatest(UPDATE_POST, sendPost);
}
