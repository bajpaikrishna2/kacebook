/*
 *
 * HomePage actions
 *
 */

import {
  FETCH_POSTS,
  POST_FETCHED,
  UPDATE_POST,
  POSTS_UPDATED,
} from './constants';

export function postFetched(data) {
  return {
    type: POST_FETCHED,
    data,
  };
}

export function updatePost(data) {
  return {
    type: UPDATE_POST,
    data,
  };
}

export function postUpdated(data) {
  return {
    type: POSTS_UPDATED,
    data,
  };
}

export function fetchPosts(data) {
  return {
    type: FETCH_POSTS,
    data,
  };
}
