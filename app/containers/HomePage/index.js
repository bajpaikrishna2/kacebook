/**
 *
 * HomePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';

import PostBox from '../../components/PostBox';
import Post from '../../components/Post';

import { selectPosts } from './selectors';
import { updatePost, fetchPosts } from './actions';
import './style.scss';

export class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { user, fetch } = this.props;
    fetch({ id: user.get('id') });
  }

  componentWillReceiveProps({ user: newUser }) {
    const { user, fetch } = this.props;
    if (!user.get('id') && newUser.get('id')) {
      fetch({ id: newUser.get('id') });
    }
  }

  addPost(post) {
    this.props.update({
      id: this.props.user.get('id'),
      post,
      type: 'new',
    });
  }

  updatePost = (data) => {
    this.props.update({
      ...data,
    });
  }

  render() {
    const { posts, user } = this.props;
    return (
      <div className="homepage-container">
        <PostBox addPost={(text) => this.addPost(text)} />
        {posts.map((post) => <Post key={post.get('id')} user={user} data={post} updatePost={this.updatePost} />)}
      </div>
    );
  }
}

HomePage.propTypes = {
  posts: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  user: PropTypes.object,
  update: PropTypes.func,
  fetch: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  posts: selectPosts(),
});

const mapDispatchToProps = {
  update: updatePost,
  fetch: fetchPosts,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'homePage', reducer });
const withSaga = injectSaga({ key: 'homePage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
