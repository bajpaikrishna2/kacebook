/*
 *
 * HomePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  POST_FETCHED,
  POSTS_UPDATED,
} from './constants';

const initialState = fromJS({
  posts: [],
});

function homePageReducer(state = initialState, action) {
  switch (action.type) {
    case POSTS_UPDATED:
    case POST_FETCHED:
      return state
      .set('posts', fromJS(action.data));
    default:
      return state;
  }
}

export default homePageReducer;
