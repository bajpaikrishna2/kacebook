import { createSelector } from 'reselect';

/**
 * Direct selector to the homePage state domain
 */
const selectHomePageDomain = (state) => state.get('homePage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by HomePage
 */

const selectPosts = () => createSelector(
  selectHomePageDomain,
  (substate) => substate.get('posts').reverse()
);

export {
  selectPosts,
};
