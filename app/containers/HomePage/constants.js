/*
 *
 * HomePage constants
 *
 */

export const FETCH_POSTS = 'app/HomePage/FETCH_POSTS';
export const POST_FETCHED = 'app/HomePage/POST_FETCHED';
export const UPDATE_POST = 'app/HomePage/UPDATE_POST';
export const POSTS_UPDATED = 'app/HomePage/POSTS_UPDATED';
