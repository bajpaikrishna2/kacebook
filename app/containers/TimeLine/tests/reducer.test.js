
import { fromJS } from 'immutable';
import timeLineReducer from '../reducer';

describe('timeLineReducer', () => {
  it('returns the initial state', () => {
    expect(timeLineReducer(undefined, {})).toEqual(fromJS({}));
  });
});
