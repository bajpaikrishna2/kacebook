import { createSelector } from 'reselect';

const selectTimeLineDomain = (state) => state.get('timeLine');

const selectPosts = () => createSelector(
  selectTimeLineDomain,
  (substate) => substate.get('posts').reverse()
);

const selectProfile = () => createSelector(
  selectTimeLineDomain,
  (substate) => substate.get('profile')
);

export {
  selectPosts,
  selectProfile,
};
