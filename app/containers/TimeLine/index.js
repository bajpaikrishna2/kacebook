/**
 *
 * TimeLine
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

import findLast from 'lodash/findLast';
import { Card } from 'reactstrap';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import Post from '../../components/Post';
import { selectPosts, selectProfile } from './selectors';
import { initProfile, updatePost, fetchPosts, reset } from './actions';
import reducer from './reducer';
import saga from './saga';
import './style.scss';

export class TimeLine extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { user, match: { params: { id } }, history, initProfile: init } = this.props;
    if (user.get('id')) {
      init(id);
    } else {
      history.push('/');
    }
  }

  componentWillReceiveProps({ posts: timeline, profile }) {
    if (!timeline.size) {
      this.props.fetchPosts({ id: profile.get('id') });
    }
  }
  componentWillUnmount() {
    this.props.reset();
  }

  getHeader=(post) => { // eslint-disable-line no-undef
    const { profile, user } = this.props;
    const participant = findLast(post.get('participants').toJS(), (part) => part.user.id === profile.get('id'));
    return participant && (`${participant.user.name === user.get('name') ? 'You' : participant.user.name} ${participant.rel}`);
  }

  updatePost=(data) => {
    this.props.updatePost({
      ...data,
    });
  }

  render() {
    const { posts, user } = this.props;
    return (
      <div className={'timeline-container'}>
        {posts.map((post) => (
          <div className={'timeline-post-container'} key={post.get('id')}>
            <Card className={'post-rel-header'}>{this.getHeader(post)}</Card>
            <Post user={user} data={post} updatePost={this.updatePost} />
          </div>
        ))}
      </div>
    );
  }
}

TimeLine.propTypes = {
  posts: PropTypes.object,
  user: PropTypes.object,
  profile: PropTypes.object,
  match: PropTypes.object,
  history: PropTypes.object,
  initProfile: PropTypes.func,
  fetchPosts: PropTypes.func,
  updatePost: PropTypes.func,
  reset: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  posts: selectPosts(),
  profile: selectProfile(),
});

const mapDispatchToProps = {
  initProfile,
  reset,
  fetchPosts,
  updatePost,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'timeLine', reducer });
const withSaga = injectSaga({ key: 'timeLine', saga });

export default compose(
  withReducer,
  withSaga,
  withRouter,
  withConnect,
)(TimeLine);
