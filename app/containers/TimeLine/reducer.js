/*
 *
 * TimeLine reducer
 *
 */

import { fromJS } from 'immutable';
import {
  TIMELINE_POST_FETCHED,
  TIMELINE_USER_FETCHED,
  TIMELINE_POSTS_UPDATED,
  RESET,
} from './constants';

const initialState = fromJS({
  profile: {},
  posts: [],
});

function timeLineReducer(state = initialState, action) {
  switch (action.type) {
    case TIMELINE_USER_FETCHED:
      return state
      .set('profile', fromJS(action.data));
    case TIMELINE_POST_FETCHED:
    case TIMELINE_POSTS_UPDATED:
      return state
      .set('posts', fromJS(action.data));
    case RESET:
      return initialState;
    default:
      return state;
  }
}

export default timeLineReducer;
