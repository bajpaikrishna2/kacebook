export const INIT_TIMELINE_USER = 'app/TimeLine/INIT_TIMELINE_USER';
export const TIMELINE_USER_FETCHED = 'app/TimeLine/TIMELINE_USER_FETCHED';
export const FETCH_TIMELINE_POSTS = 'app/TimeLine/FETCH_TIMELINE_POSTS';
export const TIMELINE_POST_FETCHED = 'app/TimeLine/TIMELINE_POST_FETCHED';
export const UPDATE_TIMELINE_POST = 'app/TimeLine/UPDATE_POST';
export const TIMELINE_POSTS_UPDATED = 'app/TimeLine/POSTS_UPDATED';
export const RESET = 'app/TimeLine/RESET';
