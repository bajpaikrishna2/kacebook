/*
 *
 * TimeLine actions
 *
 */

import {
  TIMELINE_POST_FETCHED,
  TIMELINE_USER_FETCHED,
  TIMELINE_POSTS_UPDATED,
  FETCH_TIMELINE_POSTS,
  UPDATE_TIMELINE_POST,
  INIT_TIMELINE_USER,
  RESET,
} from './constants';

export function initProfile(data) {
  return {
    type: INIT_TIMELINE_USER,
    data,
  };
}

export function profileFetched(data) {
  return {
    type: TIMELINE_USER_FETCHED,
    data,
  };
}

export function fetchPosts(data) {
  return {
    type: FETCH_TIMELINE_POSTS,
    data,
  };
}

export function postFetched(data) {
  return {
    type: TIMELINE_POST_FETCHED,
    data,
  };
}

export function updatePost(data) {
  return {
    type: UPDATE_TIMELINE_POST,
    data,
  };
}

export function postUpdated(data) {
  return {
    type: TIMELINE_POSTS_UPDATED,
    data,
  };
}

export function reset() {
  return {
    type: RESET,
  };
}
