import { takeLatest, call, put, take } from 'redux-saga/effects';
import { INIT_TIMELINE_USER, UPDATE_TIMELINE_POST, FETCH_TIMELINE_POSTS } from './constants';
import { postFetched, postUpdated, profileFetched } from './actions';
import { forServer } from '../../utils/formatMessages';
import { webSocket, createEventChannel } from '../../utils/socket';

export function* connectSocket({ data }) {
  const channel = yield call(createEventChannel, 'profile');
  setTimeout(() => {
    webSocket.send(forServer('profile', { id: data }));
  }, 0);
  while (true) { // eslint-disable-line no-constant-condition
    const response = yield take(channel);
    if (response.id) {
      yield put(profileFetched(response));
    } else {
      window.location.href = '/';
    }
  }
}

export function* sendPost({ data }) {
  const channel = yield call(createEventChannel, 'timeline');
  setTimeout(() => {
    webSocket.send(forServer('update', data));
  }, 0);
  while (true) { // eslint-disable-line no-constant-condition
    const response = yield take(channel);
    yield put(postUpdated(response));
  }
}

export function* fetchPosts({ data }) {
  const channel = yield call(createEventChannel, 'timeline');
  setTimeout(() => {
    webSocket.send(forServer('timeline', data));
  }, 0);
  while (true) { // eslint-disable-line no-constant-condition
    const response = yield take(channel);
    yield put(postFetched(response));
  }
}

export default function* defaultSaga() {
  yield takeLatest(INIT_TIMELINE_USER, connectSocket);
  yield takeLatest(FETCH_TIMELINE_POSTS, fetchPosts);
  yield takeLatest(UPDATE_TIMELINE_POST, sendPost);
}
