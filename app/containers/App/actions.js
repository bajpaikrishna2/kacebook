import {
    INIT_USER,
    USER_CREATED,
  } from './constants';

export function initUser() {
  return {
    type: INIT_USER,
  };
}

export function userCreated(data) {
  return {
    type: USER_CREATED,
    data,
  };
}
