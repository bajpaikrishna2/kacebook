import { takeLatest, call, put, take } from 'redux-saga/effects';
import { INIT_USER } from './constants';
import { userCreated } from './actions';
import { forServer } from '../../utils/formatMessages';
import { webSocket, createEventChannel } from '../../utils/socket';

export function* connectSocket() {
  const channel = yield call(createEventChannel, 'init');
  setTimeout(() => {
    webSocket.send(forServer('init'));
  }, 0);
  while (true) { // eslint-disable-line no-constant-condition
    const data = yield take(channel);
    yield put(userCreated(data));
  }
}

export default function* defaultSaga() {
  yield takeLatest(INIT_USER, connectSocket);
}
