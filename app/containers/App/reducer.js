/*
 *
 * HomePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  USER_CREATED,
} from './constants';

const initialState = fromJS({
  user: JSON.parse(sessionStorage.getItem('user')) || {},
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case USER_CREATED: {
      sessionStorage.setItem('user', JSON.stringify(action.data));
      return state
      .set('user', fromJS(action.data));
    }
    default:
      return state;
  }
}

export default appReducer;
