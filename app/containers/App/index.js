/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { Switch, Route, withRouter } from 'react-router-dom';

import HomePage from 'containers/HomePage/Loadable';
import TimeLine from 'containers/TimeLine/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

import Header from '../../components/PageHeader';
import saga from './saga';
import reducer from './reducer';
import { selectUserInfo } from './selectors';
import { initUser } from './actions';
import './style.scss';

export class App extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    const { user, init } = this.props;
    if (!user.get('id')) {
      init();
    }
  }


  render() {
    const { user, location } = this.props;
    return (
      <div className="app-container">
        {user && user.get('id') ?
        [<Header user={user} location={location} key={'page-header-element'} />,
          <main className={'main-container'} key={'main-container-element'}>
            <Switch>
              <Route exact path="/" render={() => <HomePage user={user} />} />
              <Route exact path="/:id" render={() => <TimeLine user={user} />} />
              <Route component={NotFoundPage} />
            </Switch>
          </main>]
          : 'Loading'}
      </div>
    );
  }
}

App.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  init: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  user: selectUserInfo(),
});

const mapDispatchToProps = {
  init: initUser,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'app', reducer });
const withSaga = injectSaga({ key: 'app', saga });

export default compose(
  withRouter,
  withReducer,
  withSaga,
  withConnect,
)(App);
