import { createSelector } from 'reselect';

const selectRoute = (state) => state.get('route');
const selectApp = (state) => state.get('app');

const makeSelectLocation = () => createSelector(
  selectRoute,
  (routeState) => routeState.get('location').toJS()
);

const selectUserInfo = () => createSelector(
  selectApp,
  (substate) => substate.get('user')
);

export {
  makeSelectLocation,
  selectUserInfo,
};
