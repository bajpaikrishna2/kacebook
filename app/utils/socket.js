import { eventChannel } from 'redux-saga';
import { toClient } from './formatMessages';
let ws = null;

function* createEventChannel(trxType) {
  return eventChannel((emit) => {
    /* eslint-disable no-param-reassign */
    ws.onopen = () => {
      console.log('Socket opened');
    };

    ws.onmessage = (payload) => {
      const { type, data = {} } = toClient(payload);
      if (type === trxType) {
        emit(data);
      }
    };
    /* eslint-enable no-param-reassign */
    return () => {
      ws.close();
    };
  });
}

const webSocket = (() => {
  if (ws) {
    return ws;
  }
  ws = new WebSocket('ws://localhost:3005');
  return ws;
})();

export { webSocket, createEventChannel };
