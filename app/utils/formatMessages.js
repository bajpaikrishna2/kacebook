export const forServer = (type, data = {}) => JSON.stringify({ type, data });
export const toClient = ({ data }) => JSON.parse(data);
