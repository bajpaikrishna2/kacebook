/**
*
* Comment
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import './style.scss';
import NameLink from '../NameLink';

import LikeButton from '../LikeButton';
import TimeStamp from '../TimeStamp';
import { selectUserInfo } from './selectors';


class Comment extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { comment, updateComment, user } = this.props;
    const author = comment.get('author').toJS();
    return (
      <div className={'comment-container'}>
        <div className={'comment-details'}>
          <span className={'comment-author'}><NameLink path={author.id}>{author.name}</NameLink></span>
          <span className={'comment-content'}>{comment.get('text')}</span>
        </div>
        <LikeButton mode={'comment'} status={!!comment.get('likes').toJS().find(({ id }) => id === user.get('id'))} liked={() => updateComment({ parent: { type: 'comment', id: comment.get('commentId') }, type: 'like' })} likes={comment.get('likes')} />
        <TimeStamp className={'with-dot'} date={comment.get('timestamp')} />
      </div>
    );
  }
}

Comment.propTypes = {
  comment: PropTypes.object,
  updateComment: PropTypes.func,
  user: PropTypes.object,
};


const mapStateToProps = createStructuredSelector({
  user: selectUserInfo(),
});

const withConnect = connect(mapStateToProps, null);

export default compose(withConnect)(Comment);
