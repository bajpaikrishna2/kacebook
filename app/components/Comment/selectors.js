import { createSelector } from 'reselect';

/**
 * Direct selector to the homePage state domain
 */
const selectAppDomain = (state) => state.get('app');

/**
 * Other specific selectors
 */


/**
 * Default selector used by HomePage
 */

const selectUserInfo = () => createSelector(
  selectAppDomain,
  (substate) => substate.get('user')
);

export {
  selectUserInfo,
};
