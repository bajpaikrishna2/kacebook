/**
*
* CommentBox
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Card, Button } from 'reactstrap';
import './style.scss';


class CommentBox extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.commentTextChanged = this.commentTextChanged.bind(this);
    this.state = {
      disableComment: true,
    };
    this.textarea = null;
  }

  commentTextChanged({ target: { value } }) {
    this.setState({
      disableComment: !value.trim().length,
    });
  }

  addComment() {
    const { addComment } = this.props;
    if (this.textarea.value.trim().length && addComment) {
      addComment({ text: this.textarea.value, parent: null, type: 'comment' });
      this.textarea.value = '';
      this.setState({
        disableComment: true,
      });
    }
  }

  render() {
    return (
      <Card className="comment-box">
        <textarea ref={(ele) => { this.textarea = ele; }} placeholder="Add a comment..." onChange={this.commentTextChanged}></textarea>
        <Button className={'comment-button'} disabled={this.state.disableComment} onClick={() => this.addComment()}>Comment</Button>
      </Card>
    );
  }
}

CommentBox.propTypes = {
  addComment: PropTypes.func,
};

export default CommentBox;
