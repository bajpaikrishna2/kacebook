/**
*
* CommentsList
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import Comment from '../Comment';


class CommentsList extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { comments, updateComments } = this.props;
    return (
      comments.reverse().map((comment) => <Comment key={comment.get('commentId')} comment={comment} updateComment={updateComments} />)
    );
  }
}

CommentsList.propTypes = {
  comments: PropTypes.object,
  updateComments: PropTypes.func,
};

export default CommentsList;
