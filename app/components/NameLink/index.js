/**
*
* NameLink
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

function NameLink({ children, path }) {
  return (
    <span className={'highlight'}><Link to={`/${path}`}>{children}</Link></span>
  );
}

NameLink.propTypes = {
  path: PropTypes.string,
  children: PropTypes.node,
};

export default NameLink;
