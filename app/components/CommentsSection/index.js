/**
*
* Comments
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import CommentBox from '../CommentBox';
import CommentsList from '../CommentsList';


function CommentsSection({ data, updateComments }) {
  return (
    <div className={'comments-section-container'}>
      <CommentBox addComment={updateComments} />
      <CommentsList comments={data} updateComments={updateComments} />
    </div>
  );
}

CommentsSection.propTypes = {
  data: PropTypes.object,
  updateComments: PropTypes.func,
};

export default CommentsSection;
