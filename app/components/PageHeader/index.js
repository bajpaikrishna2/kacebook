/**
*
* PageHeader
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './style.scss';

function PageHeader({ user, location: { pathname } }) {
  return (
    <header className={'page-header-container'}>
      <div>
        {
          pathname === '/' ? <span>Welcome <Link to={`/${user.get('id')}`}>{user.get('name')}</Link></span> : <Link to={'/'}><i className={'fas fa-chevron-left'} /> Go Home</Link>
        }
      </div>
    </header>
  );
}

PageHeader.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
};

export default PageHeader;
