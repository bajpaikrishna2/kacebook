/**
*
* TimeStamp
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import './style.scss';


class TimeStamp extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      date: moment.duration(moment(props.date).diff(moment())).humanize(true),
    };
    this.interval = null;
  }
  componentDidMount() {
    this.interval = setInterval(() => {
      this.setState({
        date: moment.duration(moment(this.props.date).diff(moment())).humanize(true),
      });
    }, 55000);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { className } = this.props;
    return (
      <div className={`timestamp ${className}`}>{this.state.date}</div>
    );
  }
}

TimeStamp.propTypes = {
  date: PropTypes.number,
  className: PropTypes.string,
};

export default TimeStamp;
