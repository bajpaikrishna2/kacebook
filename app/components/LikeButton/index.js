/**
*
* LikeButton
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { UncontrolledTooltip } from 'reactstrap';
import { Link } from 'react-router-dom';
import NameLink from '../NameLink';
import Icon from '../Icon';
import './style.scss';

function LikeButton({ status, liked, likes, mode = 'post' }) {
  return (
    <section className={`action-container ${mode === 'comment' && 'for-comment'}`}>
      {!!likes.size && <section className={`stats ${mode === 'comment' && 'with-dot'}`}>
        <span>
          {likes.size === 1 ?
            <span>
              <NameLink path={`${likes.getIn([0, 'id'])}`}>{likes.getIn([0, 'name'])}</NameLink> likes this
            </span> :
            <span>
              <span>
                <span className={'highlight'} id={'like'}>
                  {likes.size} people</span> like this
                </span>
              <UncontrolledTooltip autohide={false} target={'like'}>
                {
                  likes.map((member) =>
                  (<div key={member.get('id')} className={'tooltip-link'}>
                    <Link to={`/${member.get('id')}`}>
                      {member.get('name')}
                    </Link>
                  </div>))
                }
              </UncontrolledTooltip>
            </span>}
        </span>
      </section>}
      <section className={'action-buttons'}>
        <span role="presentation" className={'like-btn highlight'} onClick={liked}>
          <Icon name={`far fa-thumbs-${status ? 'down' : 'up'}`} />{status ? 'Unlike' : 'Like'}
        </span>
      </section>
    </section>
  );
}

LikeButton.propTypes = {
  likes: PropTypes.object,
  status: PropTypes.bool,
  liked: PropTypes.func,
  mode: PropTypes.string,
};

export default LikeButton;
