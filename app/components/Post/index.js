/**
*
* Post
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardHeader, CardBody } from 'reactstrap';
import TimeStamp from '../../components/TimeStamp';
import LikeButton from '../../components/LikeButton';
import NameLink from '../../components/NameLink';
import CommentsSection from '../../components/CommentsSection';
import './style.scss';

class Post extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  updatePost({ type, ...data }) {
    const { data: postData, user } = this.props;
    const userId = user.get('id');
    let obj = {
      postId: postData.get('id'),
    };
    switch (type) {
      case 'comment': {
        obj = {
          ...obj,
          ...data,
          author: user.toJS(),
          type,
        };
        break;
      }
      case 'like': {
        obj = { ...obj, type: 'like', userId, ...data };
        break;
      }
      default:
        break;
    }
    this.props.updatePost(obj);
  }

  render() {
    const { data, user } = this.props;
    const author = data.get('author').toJS();
    return (
      <Card className={'post-container'}>
        <CardHeader>
          <header><NameLink path={author.id}>{author.name}</NameLink></header>
          <TimeStamp date={data.get('timestamp')} />
        </CardHeader>
        <CardBody className={'content'}>
          <div className={'desc'}>{data.get('post')}</div>
          <section className={'engage-section'}>
            <LikeButton status={data.get('likes').toJS().find(({ id }) => id === user.get('id'))} liked={() => this.updatePost({ parent: { type: 'post' }, type: 'like' })} likes={data.get('likes')} />
            <CommentsSection data={data.get('comments')} updateComments={(commentData) => this.updatePost({ ...commentData })} />
          </section>
        </CardBody>
      </Card>
    );
  }
}

Post.propTypes = {
  user: PropTypes.object,
  data: PropTypes.object,
  updatePost: PropTypes.func,
};

export default Post;
