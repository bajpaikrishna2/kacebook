/**
*
* Icon
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

function Icon({ name }) {
  return (
    <i className={`icon-container ${name}`} />
  );
}

Icon.propTypes = {
  name: PropTypes.string,
};

export default Icon;
