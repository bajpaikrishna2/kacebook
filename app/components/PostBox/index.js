/**
*
* PostBox
*
*/

import React from 'react';
import { Card, Button } from 'reactstrap';
import PropTypes from 'prop-types';
import './style.scss';

class PostBox extends React.PureComponent {
  constructor() {
    super();
    this.postTextChanged = this.postTextChanged.bind(this);
    this.state = {
      disablePost: true,
    };
    this.textarea = null;
  }

  togglePostButton(toggleState) {
    this.setState({
      disablePost: toggleState,
    });
  }

  postTextChanged({ target: { value } }) {
    const { onChange } = this.props;
    if (value.trim().length) {
      onChange && onChange(value); // eslint-disable-line no-unused-expressions
    }
    this.togglePostButton(!value.trim().length);
  }

  addPost() {
    const { addPost } = this.props;
    addPost && addPost(this.textarea.value); // eslint-disable-line no-unused-expressions
    this.textarea.value = '';
    this.togglePostButton(true);
  }

  render() {
    return (
      <Card className="postbox">
        <textarea ref={(ele) => { this.textarea = ele; }} placeholder="Write something here..." onChange={this.postTextChanged}></textarea>
        <Button className={'post-button'} disabled={this.state.disablePost} onClick={() => this.addPost()}>Add post</Button>
      </Card>
    );
  }
}

PostBox.propTypes = {
  onChange: PropTypes.func,
  addPost: PropTypes.func,
};

export default PostBox;
