# Kacebook
This project is a fork of https://github.com/react-boilerplate/react-boilerplate
I have tweaked and removed things which I didn't need for this project and added a few modules which I needed.



### Prerequisites to run this project.
1. Node >= 6.0
2. Npm >=5.0



## Installing and getting started


#### Installation:
1. Clone the repo.
2. Go to the root of the project.
3. Run 'npm install' (or 'yarn')
4. Let everything install and ignore the warnings after install.



#### Start Project after installation
1. Go to root directory.
2. Run 'npm run start:production' to run the project in the production ENV.
3. [Optional] Run 'npm run start' to run the project in the development ENV.
4. Open your browser and visit http://localhost:3000/




## Features and Functionalities
1. When you open the project in a new tab a new user is created with the name of an [Avenger](https://en.wikipedia.org/wiki/Avengers_(comics)).
2. Default page is of the homepage where all posts can be seen.
3. You can post/comment/like.
4. If you can click on any name of the user, we go to that user's timeline in which all the posts/comments/likes of that specific user is shown.
5. Time stamp showing the elapsed time has been added.
6. Dynamic like button with tooltip of all the users who have currently liked the posts has been added.
7. Open two tabs, do something in one tab, the changes will reflect on the other tab. WebSockets have been used for this purpose.

## Built With
* [This Boilerplate](https://github.com/react-boilerplate/react-boilerplate) for the basic setup which includes:
    * React 16
    * [Redux](https://redux.js.org/)
    * [Webpack 4](https://webpack.js.org/)
    * [React Router](https://github.com/ReactTraining/react-router)
    * [ImmutableJs](https://facebook.github.io/immutable-js/)
    * [Reselect](https://github.com/reduxjs/reselect)
    * SCSS
    * NodeJS
    * ESLint

* Extra libraries used:
    * [Reactstrap](http://reactstrap.github.io/) for Bootstrap components built with React
    * Font Awesome
    * [Moment](https://momentjs.com/) for managing timestamps


Please [write](mailto:bajpaikrishna2@gmail.com) to me if you have any issues or questions regarding this assignment.