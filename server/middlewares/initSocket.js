const WebSocket = require('ws');
const WebSocketServer = WebSocket.Server;
const wss = new WebSocketServer({ port: 3005 });
const formatter = require('../utils/formatMessages');
const marvel = ['Ironman', 'Spiderman', 'Thor', 'Captain America', 'Hulk', 'Dr. Strange', 'Star Lord', 'Groot', 'Black Panther', 'Thanos'];
const posts = [];
const users = [];


const fetchPostsForUserTimeLine = (id) => { console.log(id, 'server'); return posts.filter(({ participants }) => participants.find(({ user }) => id === user.id)); };

const updatePostByTrxType = (data) => {
  const { type } = data;
  const uniqId = Date.now();
  switch (type) {
    case 'new': {
      const { post, id } = data;
      const user = users.find(({ id: userId }) => id === userId);
      posts.push({ id: uniqId, participants: [{ user, rel: 'posted' }], timestamp: uniqId, author: user, post, comments: [], likes: [] });
      break;
    }
    case 'comment': {
      const { postId, author } = data;
      posts.forEach(({ id }, i) => {
        if (id === postId) {
          posts[i].comments.push({ commentId: uniqId, likes: [], timestamp: uniqId, ...data });
          posts[i].participants.push({ user: author, rel: 'commented' });
        }
      });
      break;
    }
    case 'like': {
      const { postId, userId, parent: { type: parentType, id: parentId } } = data;
      switch (parentType) {
        case 'post': {
          posts.forEach(({ id }, i) => {
            if (id === postId) {
              let likes = posts[i].likes;
              let participants = posts[i].participants;
              const likedByCurrentUser = likes.find(({ id: usId }) => usId === userId);
              if (likedByCurrentUser) {
                likes = likes.filter(({ id: usId }) => usId !== userId);
                participants = participants.filter(({ user, rel }) => user.id !== userId || ((rel === 'posted' || rel === 'commented') && user.id === userId));
              } else {
                const user = users.find(({ id: usId }) => usId === userId);
                likes.push(user);
                participants.push({ user, rel: 'liked a post' });
              }
              posts[i].likes = likes;
              posts[i].participants = participants;
            }
          });
          break;
        }
        case 'comment': {
          const postIndex = posts.findIndex(({ id }) => id === postId);
          posts[postIndex].comments.forEach((comment, i) => {
            if (comment.commentId === parentId) {
              let likes = comment.likes;
              let participants = posts[postIndex].participants;
              const likedByCurrentUser = likes.find(({ id: usId }) => usId === userId);
              if (likedByCurrentUser) {
                likes = likes.filter(({ id: usId }) => usId !== userId);
                participants = participants.filter(({ user, rel }) => user.id !== userId || ((rel === 'posted' || rel === 'commented') && user.id === userId));
              } else {
                const user = users.find(({ id: usId }) => usId === userId);
                likes.push(user);
                participants.push({ user, rel: 'liked a comment' });
              }
              posts[postIndex].comments[i].likes = likes;
              posts[postIndex].participants = participants;
            }
          });
          break;
        }
        default:
          break;
      }
      break;
    }
    default:
      break;
  }
};

module.exports = () => {
  wss.on('connection', (ws) => {
    ws.on('message', (msg) => {
      const { type, data } = formatter.toServer(msg);
      switch (type) {
        case 'init': {
          const charIndex = parseInt(Math.random() * 10, 10) % marvel.length;
          const charId = marvel[charIndex].split(' ').map((name) => name.toLowerCase()).join('_');
          const suffix = users.filter(({ id }) => id.indexOf(charId) > -1).length;
          const newUser = {
            id: `${charId}${suffix || ''}`,
            name: `${marvel[charIndex]}${suffix ? ` of earth ${suffix}` : ''}`,
          };
          users.push(newUser);
          ws.send(formatter.forClient('init', newUser));
          break;
        }
        case 'update': {
          updatePostByTrxType(data);
          wss.clients.forEach((client) => {
            if (client.readyState === WebSocket.OPEN) {
              client.send(formatter.forClient('posts', posts));
            }
          });
          const { id: userId } = data;
          const timeline = fetchPostsForUserTimeLine(userId);
          wss.clients.forEach((client) => {
            if (client.readyState === WebSocket.OPEN) {
              client.send(formatter.forClient('timeline', timeline));
            }
          });
          break;
        }
        case 'users': {
          ws.send(formatter.forClient('users', users));
          break;
        }
        case 'profile': {
          const { id: userId } = data;
          const user = users.find(({ id }) => id === userId);
          ws.send(formatter.forClient('profile', user));
          break;
        }
        case 'timeline': {
          const { id: userId } = data;
          const timeline = fetchPostsForUserTimeLine(userId);
          wss.clients.forEach((client) => {
            if (client === ws) {
              client.send(formatter.forClient('timeline', timeline));
            }
          });
          break;
        }
        case 'posts': {
          wss.clients.forEach((client) => {
            if (client === ws) {
              client.send(formatter.forClient('posts', posts));
            }
          });
          break;
        }
        default:
          break;
      }
    });
  });
};
