module.exports = {
  forClient: (type, data) => JSON.stringify({ type, data }),
  toServer: (msg) => JSON.parse(msg),
};
